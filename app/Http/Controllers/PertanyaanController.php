<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
class PertanyaanController extends Controller
{
    public function create(){
        return view('pertanyaan/create');
    }

    public function index(){
        $post = DB::table('pertanyaan')->get();
        return view('pertanyaan.index',compact('post'));
    }

    public function store(Request $request){
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);
        
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi"=>$request["isi"]
        ]);

        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Disimpan!');
    }

    public function show($no){
        $post = DB::table('pertanyaan')->where('id',$no)->first();
        return view('pertanyaan.show',compact('post'));
    }

    public function edit($no){
        $post = DB::table('pertanyaan')->where('id',$no)->first();
        return view('pertanyaan.edit',compact('post'));
    }

    public function update($id,Request $request){
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);

        $query = DB::table('pertanyaan')
                    ->where('id',$id)
                    ->update([
                        'judul'=>$request['judul'],
                        'isi'=>$request['isi']
                    ]);
        return redirect('/pertanyaan')->with('success','Berhasil Update Pertanyaan!');
    }

    public function destroy($id){
        $query = DB::table('pertanyaan')->where('id',$id)->delete();
        return redirect('/pertanyaan')->with('success','Pertanyaan berhasil dihapus!');
    }
    
}
